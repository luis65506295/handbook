---
title: "Policies related to GitLab.com"
---

The handbook pages nested under "policies" directory are [controlled documents](https://handbook.gitlab.com/handbook/security/controlled-document-procedure/), and follow a specific set of requirements to satisfy various regulatory obligations.

Avoid nesting non-controlled documentation at this location.
